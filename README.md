# Python boilerplate

Common template for new python projects

## Install Package Dependencies

```shell
pipenv --python 3.11
pipenv shell
pipenv install --dev
```
