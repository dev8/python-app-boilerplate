import os
from src.utils.logger import init_logger
from src.utils.constants import (
    LOG_PATH,
    LOG_LEVEL,
)

if not os.path.exists(LOG_PATH):
    os.mkdir(LOG_PATH)

if LOG_LEVEL == "DEBUG":
    testing_mode = True
else:
    testing_mode = False

logger = init_logger(__name__, testing_mode=testing_mode)
