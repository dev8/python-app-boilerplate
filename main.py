from src.utils.init import logger


def main() -> None:
    logger.info("OK!")


if __name__ == "__main__":
    main()
